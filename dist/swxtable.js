(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(jQuery);
    }
}(function($) {
var TextEditor = function TextEditor(options) {
    if(!(this instanceof TextEditor)) {
        return new TextEditor(options);
    }
    if(typeof options !== 'object') {
        throw new Error('TextEditor must be initialized with param "options".');
    }
    this.options = {
        disabled: true,
        value: '',
        classes: 'swxtable__text'
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();

};

TextEditor.prototype.init = function init() {
    this.$el = $('<input type="text"/>');
    this.$el.val(this.options.value);
    this.$el.prop('disabled', this.options.disabled);
    this.$el.addClass(this.options.classes);
};

TextEditor.prototype.addListeners = function addListeners() {
    /**
     * TODO: think about a way to trigger tbody change.
     */
    this.$el.change(function() {
        console.log('changed!');
    });
};

TextEditor.prototype.render = function render() {
    return this.$el;
};

var Thead = function Thead(options) {
    if(!(this instanceof Thead)) {
        return new Thead(options);
    }
    if(typeof options !== 'object') {
        throw new Error('Thead must be initialized with param "options".');
    }
    this.options = {
        useUnit: false,
        columnTitles: [],
        units: []
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

Thead.prototype.init = function init() {
    this.$el = $('<thead/>');
    var $tr = $('<tr/>'), $th;
    this.options.columnTitles.forEach(function(title) {
        $th = $('<th/>');
        $th.html(title);
        $tr.append($th);
    });
    /**
     * TODO: Add suport to unit component.
     */
    this.$el.append($tr);
};

Thead.prototype.addListeners = function addListeners() {
    /**
     * TODO: If useUnit, add listener for change input column values.
     */
};

Thead.prototype.render = function render() {
    return this.$el;
};

var Td = function Td(options) {
    if(!(this instanceof Td)) {
        return new Td(options);
    }
    this.options = {
        editor: 'text',
        value: '',
        disabled: false
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

Td.prototype.init = function init() {
    this.$el = $('<td/>');
    switch(this.options.editor) {
        case 'text':
            this.editor = new TextEditor({
                disabled: this.options.disabled,
                value: this.options.value
            });
        break;
        default:
            this.editor = new TextEditor({
                disabled: this.options.disabled,
                value: this.options.value
            });
        break;
    }
};

Td.prototype.addListeners = function addListeners() {

};

Td.prototype.render = function render() {
    this.$el.append(this.editor.render());
    return this.$el;
};

var Tr = function Tr(options) {
    if(!(this instanceof Tr)) {
        return new Tr(options);
    }
    if(typeof options !== 'object') {
        throw new Error('Tr must be initialized with param "options".');
    }
    this.options = {
        data: {},
        columns: []
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

Tr.prototype.init = function init() {
    this.$el = $('<tr/>');
    this.tds = this.options.columns.map(function(column) {
        return new Td({
            editor: column.editor,
            value: this.options.data[column.data]
        });
    }.bind(this));
};

Tr.prototype.addListeners = function addListeners() {

};

Tr.prototype.render = function render() {
    this.$el.html('');
    this.tds.forEach(function(td) {
        this.$el.append(td.render());
    }.bind(this));
    return this.$el;
};

var Tbody = function Tbody(options) {
    if(!(this instanceof Tbody)) {
        return new Tbody(options);
    }
    if(typeof options !== 'object') {
        throw new Error('Tbody must be initialized with param "options".');
    }
    this.options = {
        data: [],
        columns: []
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

Tbody.prototype.init = function init() {
    this.$el = $('<tbody/>');
    this.trs = this.options.data.map(function(obj) {
        return new Tr({
            data: obj,
            columns: this.options.columns
        });
    }.bind(this));
};

Tbody.prototype.addListeners = function addListeners() {

};

Tbody.prototype.render = function render() {
    this.$el.html('');
    this.trs.forEach(function(tr) {
        this.$el.append(tr.render());
    }.bind(this));
    return this.$el;
};

/**
 * ```js
 * column: {
 *     title: 'titulo',
 *     name: 'nome do atributo'
 * },
 * dataT: {
 *     prop1: value1,
 *     prop2: value2,
 *     prop3: value3
 * }
 * ```
 *
 *
 * @param {object} options - Opções.
 */
var SWXTable = function SWXTable(container, options) {
    if(!(this instanceof SWXTable)) {
        return new SWXTable(options);
    }
    if(typeof options !== 'object') {
        throw new Error('SWXTable must be initialized with param "options".');
    }
    this.options = {
        data: [],
        columns: [],
        classes: 'swxtable__table'
    };
    this.container = container;
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

SWXTable.prototype.initThead = function() {
    var columnTitles = this.options.columns.map(function(column) {
        return column.name;
    });
    this.thead = new Thead({
        columnTitles: columnTitles
    });
};

SWXTable.prototype.initTbody = function() {
    this.tbody = new Tbody({
        data: this.options.data,
        columns: this.options.columns
    });
};

SWXTable.prototype.init = function init() {
    this.initThead();
    this.initTbody();
    this.$el = $('<table/>');
};

SWXTable.prototype.addListeners = function addListeners() {

};

SWXTable.prototype.start = function(){
    var table = this.render();
    $(this.container).html(table);
};

SWXTable.prototype.render = function render() {
    this.$el.html('');
    this.$el.append(this.thead.render());
    this.$el.append(this.tbody.render());
    return this.$el;
};

var swxTable = function swxTable(options) {
    var $this = $(this);
    $this.each(function(index, el) {
        var table = new SWXTable(el, options);
        table.start();
        $this.data('swxtable', table);
    });
    return $this;
};

$.SWXTable = SWXTable;
$.fn.swxTable = swxTable;
}));
