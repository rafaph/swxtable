/**
 * ```js
 * column: {
 *     title: 'titulo',
 *     name: 'nome do atributo'
 * },
 * dataT: {
 *     prop1: value1,
 *     prop2: value2,
 *     prop3: value3
 * }
 * ```
 *
 *
 * @param {object} options - Opções.
 */
var SWXTable = function SWXTable(container, options) {
    if(!(this instanceof SWXTable)) {
        return new SWXTable(options);
    }
    if(typeof options !== 'object') {
        throw new Error('SWXTable must be initialized with param "options".');
    }
    this.options = {
        data: [],
        columns: [],
        classes: 'swxtable__table'
    };
    this.container = container;
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

SWXTable.prototype.initThead = function() {
    var columnTitles = this.options.columns.map(function(column) {
        return column.name;
    });
    this.thead = new Thead({
        columnTitles: columnTitles
    });
};

SWXTable.prototype.initTbody = function() {
    this.tbody = new Tbody({
        data: this.options.data,
        columns: this.options.columns
    });
};

SWXTable.prototype.init = function init() {
    this.initThead();
    this.initTbody();
    this.$el = $('<table/>');
};

SWXTable.prototype.addListeners = function addListeners() {

};

SWXTable.prototype.start = function(){
    var table = this.render();
    $(this.container).html(table);
};

SWXTable.prototype.render = function render() {
    this.$el.html('');
    this.$el.append(this.thead.render());
    this.$el.append(this.tbody.render());
    return this.$el;
};
