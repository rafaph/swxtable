var Thead = function Thead(options) {
    if(!(this instanceof Thead)) {
        return new Thead(options);
    }
    if(typeof options !== 'object') {
        throw new Error('Thead must be initialized with param "options".');
    }
    this.options = {
        useUnit: false,
        columnTitles: [],
        units: []
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

Thead.prototype.init = function init() {
    this.$el = $('<thead/>');
    var $tr = $('<tr/>'), $th;
    this.options.columnTitles.forEach(function(title) {
        $th = $('<th/>');
        $th.html(title);
        $tr.append($th);
    });
    /**
     * TODO: Add suport to unit component.
     */
    this.$el.append($tr);
};

Thead.prototype.addListeners = function addListeners() {
    /**
     * TODO: If useUnit, add listener for change input column values.
     */
};

Thead.prototype.render = function render() {
    return this.$el;
};
