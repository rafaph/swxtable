var Tr = function Tr(options) {
    if(!(this instanceof Tr)) {
        return new Tr(options);
    }
    if(typeof options !== 'object') {
        throw new Error('Tr must be initialized with param "options".');
    }
    this.options = {
        data: {},
        columns: []
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

Tr.prototype.init = function init() {
    this.$el = $('<tr/>');
    this.tds = this.options.columns.map(function(column) {
        return new Td({
            editor: column.editor,
            value: this.options.data[column.data]
        });
    }.bind(this));
};

Tr.prototype.addListeners = function addListeners() {

};

Tr.prototype.render = function render() {
    this.$el.html('');
    this.tds.forEach(function(td) {
        this.$el.append(td.render());
    }.bind(this));
    return this.$el;
};
