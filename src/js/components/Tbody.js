var Tbody = function Tbody(options) {
    if(!(this instanceof Tbody)) {
        return new Tbody(options);
    }
    if(typeof options !== 'object') {
        throw new Error('Tbody must be initialized with param "options".');
    }
    this.options = {
        data: [],
        columns: []
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

Tbody.prototype.init = function init() {
    this.$el = $('<tbody/>');
    this.trs = this.options.data.map(function(obj) {
        return new Tr({
            data: obj,
            columns: this.options.columns
        });
    }.bind(this));
};

Tbody.prototype.addListeners = function addListeners() {

};

Tbody.prototype.render = function render() {
    this.$el.html('');
    this.trs.forEach(function(tr) {
        this.$el.append(tr.render());
    }.bind(this));
    return this.$el;
};
