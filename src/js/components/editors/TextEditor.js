var TextEditor = function TextEditor(options) {
    if(!(this instanceof TextEditor)) {
        return new TextEditor(options);
    }
    if(typeof options !== 'object') {
        throw new Error('TextEditor must be initialized with param "options".');
    }
    this.options = {
        disabled: true,
        value: '',
        classes: 'swxtable__text'
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();

};

TextEditor.prototype.init = function init() {
    this.$el = $('<input type="text"/>');
    this.$el.val(this.options.value);
    this.$el.prop('disabled', this.options.disabled);
    this.$el.addClass(this.options.classes);
};

TextEditor.prototype.addListeners = function addListeners() {
    /**
     * TODO: think about a way to trigger tbody change.
     */
    this.$el.change(function() {
        console.log('changed!');
    });
};

TextEditor.prototype.render = function render() {
    return this.$el;
};
