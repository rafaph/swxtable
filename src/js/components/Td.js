var Td = function Td(options) {
    if(!(this instanceof Td)) {
        return new Td(options);
    }
    this.options = {
        editor: 'text',
        value: '',
        disabled: false
    };
    $.extend(this.options, options);
    this.init();
    this.addListeners();
};

Td.prototype.init = function init() {
    this.$el = $('<td/>');
    switch(this.options.editor) {
        case 'text':
            this.editor = new TextEditor({
                disabled: this.options.disabled,
                value: this.options.value
            });
        break;
        default:
            this.editor = new TextEditor({
                disabled: this.options.disabled,
                value: this.options.value
            });
        break;
    }
};

Td.prototype.addListeners = function addListeners() {

};

Td.prototype.render = function render() {
    this.$el.append(this.editor.render());
    return this.$el;
};
