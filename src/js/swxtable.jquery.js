var swxTable = function swxTable(options) {
    var $this = $(this);
    $this.each(function(index, el) {
        var table = new SWXTable(el, options);
        table.start();
        $this.data('swxtable', table);
    });
    return $this;
};
