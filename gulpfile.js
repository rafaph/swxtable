'use strict';

var nodePath = require('path');
var gulp = require('gulp');
var del = require('del');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var minify = require('gulp-minify-css');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var umd = require('gulp-umd');

var path = {
    css: {
        src: 'src/sass/',
        dest: 'dist',
        out: 'swxtable.css',
        minifiedOut: 'swxtable.min.css',
        files: [
            'swxtable.scss'
        ]
    },
    js: {
        src: 'src/js/',
        dest: 'dist',
        out: 'swxtable.js',
        minifiedOut: 'swxtable.min.js',
        files: [
            'components/editors/*.js',
            'components/Thead.js',
            'components/Td.js',
            'components/Tr.js',
            'components/Tbody.js',
            'SWXTable.js',
            'swxtable.jquery.js'
        ]
    }
};

for(var i in path.css.files) {
    path.css.files[i] = path.css.src + path.css.files[i];
}

for(i in path.js.files) {
    path.js.files[i] = path.js.src + path.js.files[i];
}

gulp.task('lint', ['build:js'], function () {
    return gulp.src(path.js.dest + '/swxtable.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default', {
        verbose: true
    }));
});

gulp.task('clean:js', function (cb) {
    del([path.js.dest + '/*.js'], cb);
});

gulp.task('clean:css', function (cb) {
    del([path.css.dest + '/*.css'], cb);
});

gulp.task('clean', ['clean:js', 'clean:css']);

gulp.task('build:js', ['clean:js'], function () {
    return gulp.src(path.js.files)
    .pipe(concat(path.js.out))
    .pipe(umd({
        exports: function (file) {
            return 'swxTable';
        },
        namespace: function () {
            return 'swxTable';
        },
        template: nodePath.join(__dirname, '/src/js/umd-template.js')
    }))
    .pipe(gulp.dest(path.js.dest))
    .pipe(rename(path.js.minifiedOut))
    .pipe(uglify())
    .pipe(gulp.dest(path.js.dest));
});

gulp.task('build:css', ['clean:css'], function () {
   return gulp.src(path.css.files)
   .pipe(sass())
   .pipe(gulp.dest(path.css.dest))
   .pipe(rename(path.css.minifiedOut))
   .pipe(minify())
   .pipe(gulp.dest(path.css.dest));
});

gulp.task('build', ['clean', 'build:js', 'build:css', 'lint']);

gulp.task('watch', ['build'], function() {
    gulp.watch(path.js.src + '*.js', ['build:js']);
    gulp.watch(path.css.src + '*.scss', ['build:css']);
});
